using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySwitcher : MonoBehaviour
{
    private const float GRAVITY = 9.81f;

    [SerializeField] private bool invertPlayer = false;
    [SerializeField] private float gravityMultiplier = 0.5f;
    [SerializeField] private GameObject player;
    [SerializeField] private PlatformEffector2D[] platformEffectors;

    private float horizontal;
    private float vertical;
    private Vector3 playerBasePos;

    private void OnEnable()
    {
        playerBasePos = player.transform.position;
    }

    private void Update()
    {
        ChangeGravityDirection(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        ManagePlayerAnimations(
            Input.GetAxis("Horizontal") == 0 ? 0 : Input.GetAxis("Horizontal") < 0 ? - 1 : 1,
            Input.GetAxis("Vertical") == 0 ? 0 : Input.GetAxis("Vertical") < 0 ? -1 : 1
        );
    }

    private void ChangeGravityDirection(float horizontalInput, float verticalInput)
    {
        if (horizontalInput == 0){
            horizontal = 0;
        }
        else if (horizontalInput < 0){
            horizontal = -GRAVITY;
        }
        else {
            horizontal = GRAVITY;
        }

        if (verticalInput == 0){
            vertical = 0;
            ManagePlatformEffectors(false);
        }
        else if (verticalInput < 0){
            vertical = -GRAVITY;
            ManagePlatformEffectors(true, true);
        }
        else {
            vertical = GRAVITY;
            ManagePlatformEffectors(true, false);
        }

        Physics2D.gravity = new Vector2(horizontal, vertical) * gravityMultiplier;
    }

    private void ManagePlatformEffectors(bool active, bool facingUp = true)
    {
        foreach (PlatformEffector2D platform in platformEffectors)
        {
            platform.enabled = active;
            if (!active) continue;

            platform.rotationalOffset = facingUp ? 0 : 180;
        }
    }

    private void ManagePlayerAnimations(int horizontalInput, int verticalInput)
    {
        int currentOrientation = -1;
        if (horizontalInput == 0 && verticalInput == 0) currentOrientation = 0;
        else if (horizontalInput == 0 && verticalInput == -1) currentOrientation = 0;
        else if(horizontalInput == 1 && verticalInput == -1) currentOrientation = 1;
        else if(horizontalInput == 1 && verticalInput == 0) currentOrientation = 2;
        else if(horizontalInput == 1 && verticalInput == 1) currentOrientation = 3;
        else if(horizontalInput == 0 && verticalInput == 1) currentOrientation = 4;
        else if(horizontalInput == -1 && verticalInput == 1) currentOrientation = 5;
        else if(horizontalInput == -1 && verticalInput == 0) currentOrientation = 6;
        else if(horizontalInput == -1 && verticalInput == -1) currentOrientation = 7;

        player.transform.eulerAngles = new Vector3(0, 0, invertPlayer ? currentOrientation * 45 + 180 : currentOrientation * 45);
    }

    private void OnDisable()
    {
        Physics2D.gravity = new Vector2(0, -GRAVITY);
        player.transform.position = playerBasePos;
    }
}
