using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotDirectionModifier : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject sprite;
    [SerializeField] private float rotationSpeed = 20f;
    [SerializeField] private float maxRotation = 120f;
    [SerializeField] private float ballSpeed = 120f;
    [SerializeField] private float ballRotationSpeed = 10f;
    [SerializeField] private float timeBetweenShots = 1f;

    [SerializeField] private Rigidbody2D playerRB;
    [SerializeField] private SpriteRenderer arrowSprite;

    private Vector3 arrowBasePos;
    private Vector3 arrowBaseRotation;

    private float timer = 0;
    private float currentRotation;
    private bool startRotating = false;
    private int modifier = 1;
    private Vector3 playerBasePos;

    private void OnEnable()
    {
        startRotating = true;
        currentRotation = maxRotation / 2;
        playerBasePos = player.transform.position;
        arrowBasePos = transform.position;
        arrowBaseRotation = transform.eulerAngles;
        playerRB = player.GetComponent<Rigidbody2D>();
        arrowSprite = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (!startRotating) return;

        RotateArrow();
        CheckForBalllaunch();
        RotateBall();
    }

    private void RotateArrow()
    {
        if (currentRotation >= maxRotation)
        {
            currentRotation = maxRotation;
            modifier = -1;
        }
        if (currentRotation == 0)
        {
            currentRotation = 0;
            modifier = 1;
        }

        float addedRotation = rotationSpeed * Time.deltaTime * modifier;
        currentRotation += addedRotation;
        currentRotation = Mathf.Clamp(currentRotation, 0, maxRotation);

        transform.RotateAround(player.transform.position, Vector3.forward, addedRotation);
    }

    private void CheckForBalllaunch()
    {
        if (Input.GetButtonDown("Jump") && timer <= 0.0f)
        {
            playerRB.velocity = Vector2.zero;
            playerRB.AddForce((transform.position - player.transform.position).normalized * ballSpeed, ForceMode2D.Impulse);
            timer = timeBetweenShots;
            arrowSprite.color = Color.red;
        } 
        else if(timer > 0.0f)
        {
            timer -= Time.deltaTime;
            if (timer <= 0.0f) arrowSprite.color = Color.white;
        }
    }

    private void RotateBall()
    {
        sprite.transform.Rotate(Vector3.forward, ballRotationSpeed * playerRB.velocity.x + playerRB.velocity.y, Space.Self);
    }

    private void OnDisable()
    {
        startRotating = false;
        sprite.transform.eulerAngles = new Vector3(0, 0, 0);
        player.transform.position = playerBasePos;
        transform.position = arrowBasePos;
        transform.eulerAngles = arrowBaseRotation;
    }
}
