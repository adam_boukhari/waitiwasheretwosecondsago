using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDisappear : MonoBehaviour, JumpObserver
{
    private bool activeOnStart;
    private void OnEnable()
    {
        activeOnStart = gameObject.transform.GetChild(0).gameObject.activeSelf;
    }

    private void OnDisable()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(activeOnStart);
    }

    public void ActionOnJump()
    {
        GameObject child = gameObject.transform.GetChild(0).gameObject;
        child.SetActive(!child.activeSelf);
    }
}
