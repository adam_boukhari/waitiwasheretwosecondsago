using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class LeverDoorActivator : MonoBehaviour
{
    [SerializeField] private DoorManager door;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Light2D light2D;
    [SerializeField] private Color deactivatedColor;
    [SerializeField] private Color activatedColor;
    private bool activated;

    private void OnEnable()
    {
        activated = false;
        light2D.color = deactivatedColor;
        spriteRenderer.flipX = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !activated)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
            activated = !activated;
            door.ActivateDoor();
            if (activated)
                light2D.color = activatedColor;
            else
                light2D.color = deactivatedColor;
        }
    }
}
