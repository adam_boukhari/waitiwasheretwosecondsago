using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkPlatform : MonoBehaviour
{
    public enum Origine { DROITE, GAUCHE, CENTRE };

    [SerializeField] private Origine shrinkOrigin;
    [SerializeField] private float shrinkSpeed;
    [SerializeField] private float maxScale;
    [SerializeField] private float minScale;
    private float baseScale;
    private Vector3 basePosition;

    void OnEnable()
    {
        baseScale = transform.localScale.x;
        basePosition = transform.position;
        InvokeRepeating("Shrink", 0, 0.005f);
    }

    private void OnDisable()
    {
        CancelInvoke("Shrink");
        transform.localScale = new Vector3(baseScale, baseScale, transform.position.z);
        transform.position = basePosition;
    }

    private void Shrink()
    {
        float newScale = transform.localScale.x - shrinkSpeed;
        if (newScale < minScale)
            newScale = minScale;
        else if (newScale > maxScale)
            newScale = maxScale;
        transform.localScale = new Vector3(newScale, transform.localScale.y, transform.localScale.z);
        if(newScale != minScale && newScale != maxScale)
        {
            if (shrinkOrigin == Origine.GAUCHE)
            {
                transform.position = new Vector3(transform.position.x - shrinkSpeed/1.4f, transform.position.y, transform.position.z);
            }
            if (shrinkOrigin == Origine.DROITE)
            {
                transform.position = new Vector3(transform.position.x + shrinkSpeed / 1.4f, transform.position.y, transform.position.z);
            }
        }
    }
}
