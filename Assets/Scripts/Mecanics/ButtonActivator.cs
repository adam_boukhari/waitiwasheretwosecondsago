using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonActivator : MonoBehaviour
{
    [SerializeField] private UnityEvent activateEvent;
    [SerializeField] private UnityEvent deactivateEvent;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Sprite activatedSprite;
    [SerializeField] private Sprite deactivatedSprite;
    private int nbOfCollisions = 0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("PlayerCorpse"))
        {
            if (nbOfCollisions == 0)
            {
                activateEvent.Invoke();
                spriteRenderer.sprite = activatedSprite;
            }

            nbOfCollisions++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("PlayerCorpse"))
        {
            nbOfCollisions--;
            nbOfCollisions = Mathf.Max(nbOfCollisions, 0);

            if (nbOfCollisions == 0)
            {
                deactivateEvent.Invoke();
                spriteRenderer.sprite = deactivatedSprite;
            }
        }
    }

    private void OnDisable()
    {
        spriteRenderer.sprite = deactivatedSprite;
        nbOfCollisions = 0;
    }
}
