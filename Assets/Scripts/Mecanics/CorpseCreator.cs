using System.Collections.Generic;
using UnityEngine;

public class CorpseCreator : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject corpsePrefab;
    [SerializeField] private LevelStarter levelStarter;

    private Rigidbody2D playerRigidBody;
    private List<GameObject> corpseList;
    private bool corpseSpawnedOnThisReset = false;

    private void Awake()
    {
        corpseList = new List<GameObject>();
    }

    private void OnEnable()
    {
        playerRigidBody = player.GetComponent<Rigidbody2D>();
        levelStarter.OnLevelStarted += ActivateCorpseCreation;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !corpseSpawnedOnThisReset)
        {
            SpawnCorpse();
        }
    }

    private void SpawnCorpse()
    {
        corpseSpawnedOnThisReset = true;
        GameObject corpse = Instantiate(corpsePrefab, player.transform.position, player.transform.rotation);
        corpse.GetComponent<Rigidbody2D>().velocity = playerRigidBody.velocity;
        corpse.GetComponentInChildren<SpriteRenderer>().flipX = playerRigidBody.velocity.x < 0;
        corpseList.Add(corpse);

        player.SetActive(false);
    }

    private void ActivateCorpseCreation()
    {
        corpseSpawnedOnThisReset = false;
    }

    public void ClearCorpses()
    {
        foreach(GameObject corpse in corpseList)
        {
            Destroy(corpse);
        }
        corpseList.Clear();
    }

    private void OnDisable()
    {
        levelStarter.OnLevelStarted -= ActivateCorpseCreation;
    }
}
