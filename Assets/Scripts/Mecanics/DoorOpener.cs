using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    [SerializeField] private int nbOfLevers = 0;
    [SerializeField] private float openTime = 2f;
   
    private Vector3 originalDoorPos;
    private Vector3 targetDoorPos;
    private float timer = 0;
    private int activatedLevers = 0;
    private bool startOpening = false;


    private void OnEnable()
    {
        float distance = GetComponent<SpriteRenderer>().bounds.size.y;
        originalDoorPos = transform.position;
        targetDoorPos = new Vector2(originalDoorPos.x, originalDoorPos.y + distance);
    }

    private void Update()
    {
        if (startOpening) timer += Time.deltaTime;
        else timer -= Time.deltaTime;

        timer = Mathf.Clamp(timer, 0, openTime);

        if (timer == 0 || timer == openTime) return;
        transform.position = Vector2.Lerp(originalDoorPos, targetDoorPos, timer / openTime);
    }

    private void OnDisable()
    {
        transform.position = originalDoorPos;
        startOpening = false;
        timer = 0;
        activatedLevers = 0;
    }

    public void ActivateActivator()
    {
        activatedLevers++;
        if (activatedLevers >= nbOfLevers) startOpening = true;
    }

    public void DeactivateActivator()
    {
        activatedLevers--;
        activatedLevers = Mathf.Max(activatedLevers, 0);
        if (activatedLevers < nbOfLevers) startOpening = false;
    }
}
