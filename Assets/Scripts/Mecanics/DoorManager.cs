using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    [SerializeField] private GameObject nextDoor;
    [SerializeField] private Sprite openedDoor;
    [SerializeField] private Sprite closedDoor;
    [SerializeField] private bool isActive;
    private bool activeOnStart;
    private bool canTeleport = true;

    private void OnEnable()
    {
        activeOnStart = isActive;
    }

    private void OnDisable()
    {
        isActive = activeOnStart;
        if (isActive)
            GetComponent<SpriteRenderer>().sprite = openedDoor;
        else
            GetComponent<SpriteRenderer>().sprite = closedDoor;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(isActive && canTeleport && nextDoor && collision.tag == "Player")
        {
            nextDoor.GetComponent<DoorManager>().teleport(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        canTeleport = true;
    }

    public void ActivateDoor()
    {
        isActive = !isActive;

        if (isActive)
            GetComponent<SpriteRenderer>().sprite = openedDoor;
        else
            GetComponent<SpriteRenderer>().sprite = closedDoor;
    }

    public void teleport(GameObject player)
    {
        canTeleport = false;
        player.transform.position = gameObject.transform.position;
    }
}
