using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSpawner : MonoBehaviour
{
    [SerializeField] private float spawnRate = 0.5f;
    private List<GameObject> traps = new List<GameObject>();
    private int trapIndex;

    private void OnEnable()
    {
        InvokeRepeating("SpawnTrap", 0.6f, spawnRate);
        trapIndex = 0;
        int childIndex = 0;
        while(childIndex != gameObject.transform.childCount)
        {
            traps.Add(gameObject.transform.GetChild(childIndex).gameObject);
            gameObject.transform.GetChild(childIndex).gameObject.SetActive(false);
            childIndex++;
        }
    }

    private void OnDisable()
    {
        CancelInvoke("SpawnTrap");
    }

    private void SpawnTrap()
    {
        if(trapIndex <= traps.Count - 1)
        {
            traps[trapIndex].SetActive(true);
            trapIndex++;
        }
    }
}
