using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakerManager : MonoBehaviour
{
    private bool inputPressed;
    private Transform itemTR;
    private Rigidbody2D itemRB;
    private int nbItemInZone;

    [SerializeField] private Vector2 heldLocation;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void FixedUpdate()
    {
    }

    private void HandleInput()
    {
        if ((Input.GetKeyDown(KeyCode.Z)|| Input.GetKeyDown(KeyCode.E))&& itemTR)
        {
            inputPressed = !inputPressed;
            print(inputPressed);
        }
          
    }
    private void OnEnable()
    {
        inputPressed = false;
    }

    private void Take()
    {
        if (inputPressed)
        {
            if (itemTR)
            {
                itemTR.position = transform.GetChild(0).transform.position;
                itemRB.velocity = new Vector3(itemRB.velocity.x, 0);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            if (nbItemInZone == 0)
            {
                itemRB = collision.gameObject.GetComponent<Rigidbody2D>();
                itemTR = collision.gameObject.transform;
            }
            nbItemInZone++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            nbItemInZone--;
            if (nbItemInZone == 0)
            {
                itemRB = null;
                itemTR = null;
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        HandleInput();
        Take();
    }
}
