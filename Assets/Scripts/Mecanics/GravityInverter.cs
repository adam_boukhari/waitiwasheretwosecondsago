using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityInverter : MonoBehaviour
{
    private const float GRAVITY = 9.81f;

    [SerializeField] private GameObject player;
    [SerializeField] private PlatformEffector2D[] platformEffectors;
    [SerializeField] private float rotationDuration = 1.0f;
    private float clock = 0f;

    public void StartGravityInversion()
    {
        StartCoroutine(InvertGravity());
    }

    private IEnumerator InvertGravity()
    {
        Physics2D.gravity = new Vector2(0, GRAVITY);

        foreach(PlatformEffector2D platform in platformEffectors)
        {
            platform.rotationalOffset = 180;
        }

        while (clock < rotationDuration)
        {
            player.transform.eulerAngles = new Vector3(0, 0, Mathf.Clamp(Mathf.LerpAngle(0, 180, clock / rotationDuration), 0, 180));
            yield return new WaitForSeconds(Time.deltaTime);
            clock += Time.deltaTime;
        }
    }

    private void OnDisable()
    {
        Physics2D.gravity = new Vector2(0, -GRAVITY);

        foreach (PlatformEffector2D platform in platformEffectors)
        {
            platform.rotationalOffset = 0;
        }
        player.transform.eulerAngles = new Vector3(0, 0, 0);
        clock = 0.0f;
    }
}
