using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeirdCameraBehaviour : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] GameObject player;
    [SerializeField] GameObject endTrigger;
    [SerializeField] float intensityMultiplier = 1.0f;
    [SerializeField] float maxXRotation = 2.0f;
    [SerializeField] float maxYRotation = 2.0f;

    private void OnEnable()
    {
        if (cam == null) cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.orthographic = false;
        cam.fieldOfView = 10.0f;
    }

    private void Update()
    {
        Vector3 playerPos = player.transform.position;
        playerPos.z = -10;
        cam.transform.position = playerPos;

        UpdateWeirdnessIntensity();
    }

    private void UpdateWeirdnessIntensity()
    {
        Vector3 rotation = cam.transform.eulerAngles;
        rotation.x = ClampAngleValue(rotation.x + GetRandomAngleIncrement(), maxXRotation);
        rotation.y = ClampAngleValue(rotation.y + GetRandomAngleIncrement(), maxYRotation);
        rotation.z += GetRandomAngleIncrement();

        cam.transform.eulerAngles = rotation;
    }

    private float GetRandomAngleIncrement()
    {
        float intensity = 1 / Mathf.Max(Vector2.Distance(player.transform.position, endTrigger.transform.position), 0.01f);
        return Random.Range(-intensityMultiplier, intensityMultiplier) * intensity * Time.deltaTime;
    }

    private float ClampAngleValue(float axisValue, float maxRotation)
    {
        if (axisValue > maxRotation) Mathf.Max(axisValue, 360 - maxRotation);
        else axisValue = Mathf.Min(axisValue, maxRotation);

        return axisValue;
    }

    private void OnDisable()
    {
        if (cam == null) return;

        cam.orthographic = true;
        cam.orthographicSize = 7;
        cam.transform.position = new Vector3(0, 0, -10);
        cam.transform.eulerAngles = new Vector3(0, 0, 0);
    }
}
