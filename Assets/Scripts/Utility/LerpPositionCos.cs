using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpPositionCos : MonoBehaviour
{
    private Vector2 PointAPos;
    private Vector2 PointBPos;
    private float lerpValue;
    private float cosLerpValue;
    private bool isGoing = true;

    [SerializeField] private float timeToLerp = 1;
    [SerializeField] private float initialLerp;

    private void Awake()
    {
        initialLerp = Mathf.Clamp(initialLerp, 0, 0.99f);
        lerpValue = initialLerp;
        PointAPos = gameObject.transform.position;
        PointBPos = gameObject.transform.GetChild(0).transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void move()
    {
        if (isGoing)
        {
            lerpValue +=  Time.deltaTime/ timeToLerp;
        }
        else
        {
            lerpValue -=  Time.deltaTime/ timeToLerp;
        }
        if(lerpValue>=1)
        {
            isGoing = false;
        }
        if (lerpValue <= 0)
        {
            isGoing = true;
        }

        cosLerpValue = (-1*Mathf.Cos(lerpValue*Mathf.PI)+1)/2;

        gameObject.transform.position = Vector2.Lerp(PointAPos, PointBPos, cosLerpValue); 
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }
}
