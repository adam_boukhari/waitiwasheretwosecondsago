using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActiveInvoker : MonoBehaviour
{

    [SerializeField] private UnityEvent onActiveEvents;
    [SerializeField] private UnityEvent onDeactiveEvents;

    private void OnEnable()
    {
        onActiveEvents.Invoke();
    }
    private void OnDisable()
    {
        onDeactiveEvents.Invoke();
    }
}
