using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFlipAuto : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spr;

    private float lastXPosition;

    // Start is called before the first frame update
    void Start()
    {
        lastXPosition = transform.position.x;
    }

    private void ManageFlip()
    {
        if (lastXPosition - transform.position.x!=0)
            spr.flipX = lastXPosition < transform.position.x;
        lastXPosition = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        ManageFlip();
    }
}
