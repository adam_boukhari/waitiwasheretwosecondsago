using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class EndLevelTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent cleanUpEvent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            LevelManager.Instance.LoadNextLevel();
            if (cleanUpEvent != null) cleanUpEvent.Invoke();
        }
    }
}
