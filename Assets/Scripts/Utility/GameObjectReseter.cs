using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectReseter : MonoBehaviour
{
    [SerializeField] private Vector2 position;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void resetPosition()
    {
        transform.position = position;
    }
}
