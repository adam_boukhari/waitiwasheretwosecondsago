using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionRecorder : MonoBehaviour
{
    [SerializeField] private PlayerReplicaManager playerReplicaManager;
    private List<Vector3> recordedPositions;
    private bool remove = false;

    private void OnEnable()
    {
        recordedPositions = new List<Vector3>();
    }

    private void FixedUpdate()
    {
        if (!playerReplicaManager.IsSpawningStarted()) return;
        recordedPositions.Add(gameObject.transform.position);

        if (remove)
            recordedPositions.RemoveAt(0);
    }

    public List<Vector3> GetRecordedPositions()
    {
        return recordedPositions;
    }

    public void StartRemovingOlderValues()
    {
        remove = true;
    }

    private void OnDisable()
    {
        recordedPositions.Clear();
        remove = false;
        gameObject.SetActive(false);
    }
}
