using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReplicaManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject replicaPrefab;
    [SerializeField] private int replicasQuantity = 10;
    [SerializeField] private float replicasSpawnRate = 1.0f;

    private PositionRecorder positionRecorder;
    private Vector3 basePlayerPosition;
    private Quaternion basePlayerRotation;
    private List<GameObject> replicas;
    private int currentReplicaIndex = 0;
    private bool spawningStarted = false;

    private void OnEnable()
    {
        if(replicaPrefab == null){
            //TODO Switch level or better null check managing
            Debug.LogError("No replica prefab");
        }

        positionRecorder = player.GetComponent<PositionRecorder>();
        basePlayerPosition = player.transform.position;
        basePlayerRotation = player.transform.rotation;

        CreateReplicas();
    }

    private void CreateReplicas()
    {
        replicas = new List<GameObject>();
        for (int i = 0; i < replicasQuantity; i++){
            GameObject replica = Instantiate(replicaPrefab, basePlayerPosition, basePlayerRotation);
            replicas.Add(replica);
            replica.GetComponent<ReplicaMovementManager>().SetPositionRecorder(positionRecorder);
            replica.SetActive(false);
        }
    }

    public IEnumerator SpawnReplica()
    {
        spawningStarted = true;
        yield return new WaitForSeconds(replicasSpawnRate);

        replicas[currentReplicaIndex].SetActive(true);
        currentReplicaIndex++;

        if (currentReplicaIndex < replicas.Count)
            StartCoroutine(SpawnReplica());
        else
        {
            positionRecorder.StartRemovingOlderValues();
            foreach (GameObject replica in replicas){
                replica.GetComponent<ReplicaMovementManager>().RemovingHasStarted();
            }
        }
    }

    public bool IsSpawningStarted()
    {
        return spawningStarted;
    }

    private void OnDisable()
    {
        foreach(GameObject replica in replicas){
            Destroy(replica);
        }
        replicas.Clear();
        spawningStarted = false;
        currentReplicaIndex = 0;
    }
}
