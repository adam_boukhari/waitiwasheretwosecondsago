using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplicaMovementManager : MonoBehaviour
{
    private AnimationManager anim = null;
    private PositionRecorder positionRecorder;
    private List<Vector3> recordedPositions;
    private int currentPositionIndex = 0;
    private bool beganRemovingOldPositions = false;
    private Vector3 lastPosition;

    private void Awake()
    {
        anim = GetComponent<AnimationManager>();
    }

    private void FixedUpdate()
    {
        HandlePosition();
    }

    private void HandlePosition()
    {
        if(recordedPositions == null) recordedPositions = positionRecorder.GetRecordedPositions();

        if (currentPositionIndex >= recordedPositions.Count)
            return;

        Vector3 currentPosition = recordedPositions[currentPositionIndex];
        if (!beganRemovingOldPositions) currentPositionIndex++;

        anim.SetDirection(currentPosition.x - lastPosition.x);
        transform.position = currentPosition;

        lastPosition = currentPosition;
    }

    public void SetPositionRecorder(PositionRecorder positionRecorder)
    {
        this.positionRecorder = positionRecorder;
    }

    public void RemovingHasStarted()
    {
        beganRemovingOldPositions = true;
    }
}
