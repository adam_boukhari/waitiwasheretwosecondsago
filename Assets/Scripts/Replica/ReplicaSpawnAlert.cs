using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplicaSpawnAlert : MonoBehaviour
{
    [SerializeField] private PlayerReplicaManager playerReplicaManager;

    private void Update()
    {
        if (playerReplicaManager.IsSpawningStarted()) return;
        if(Input.GetAxis("Horizontal") != 0 || Input.GetButtonDown("Jump") || Input.GetKey(KeyCode.LeftShift)){
            StartCoroutine(playerReplicaManager.SpawnReplica());
        }
    }
}
