using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class AudioAnalyserAnim : MonoBehaviour
{
    private const int SAMPLE_SIZE = 1024;
    [SerializeField] private float rmsValue;
    [SerializeField] private float dbValue;
    [SerializeField] private float pitchValue;

    [SerializeField] private AudioSource source;

    private float[] samples;
    private float[] spectrum;
    private float sampleRate;

    [SerializeField] private Light2D[] SpectrumLights;
    [SerializeField] private Light2D[] PitchLights;


    // Start is called before the first frame update
    void Start()
    {
        samples = new float[SAMPLE_SIZE];
        spectrum = new float[SAMPLE_SIZE];
        sampleRate = AudioSettings.outputSampleRate;
    }

    void Update()
    {
        AnalyzeSound();
        UpdateVisual();
    }

    private float lastIntensity = 0f;
    private const float INTENSITY_DIFF_THRESHOLD = 0.1f;

    private void UpdateVisual()
    {
        int visualIndex = 0;
        int spectrumIndex = 0;
        float sum = 0;


        for (int i = 0; i < 100; i ++)
        {
            sum += spectrum[i];
        }
        float intensity = (sum / 2)*3;

        float diff = Mathf.Abs(intensity - lastIntensity);

        if (diff > INTENSITY_DIFF_THRESHOLD)
            intensity /= 2;

        lastIntensity = intensity;

        for (int j =0; j< SpectrumLights.Length; j++)
        {
            SpectrumLights[j].intensity = intensity;
        }

        foreach(Light2D light in PitchLights)
        {
            light.intensity = Mathf.Clamp(0, 0.2f, pitchValue / 1000);
        }
    }

    private void AnalyzeSound()
    {
        source.GetOutputData(samples, 0);

        // Get the RMS
        int i = 0;
        float sum = 0;
        for(i=0; i< SAMPLE_SIZE; i++)
        {
            sum = samples[i] * samples[i];
        }
        rmsValue = Mathf.Sqrt(sum / SAMPLE_SIZE);
        //Get the DB value

        dbValue = 20 * Mathf.Log10(rmsValue / 0.1f);

        //Get sound spectrum
        source.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);

        //Find pitch
        float maxV = 0;
        var maxN = 0;
        for (i = 0; i < SAMPLE_SIZE; i++)
        {
            if (!(spectrum[i] > maxV) || !(spectrum[i] > 0.0f))
                continue;
            maxV = spectrum[i];
            maxN = i;
        }

        float freqN = maxN;
        if(maxN>0 && maxN < SAMPLE_SIZE - 1)
        {
            var dL = spectrum[maxN - 1] / spectrum[maxN];
            var dR = spectrum[maxN + 1] / spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        pitchValue = freqN * (sampleRate / 2) / SAMPLE_SIZE;
    }
}
