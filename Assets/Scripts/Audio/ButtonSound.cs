using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ButtonSound : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource = null;

    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
    }

    public void OnSelect(BaseEventData eventData)
    {
        AudioClip clip = Sounds.select;
        audioSource.PlayOneShot(clip);
    }

    private void OnClick()
    {
        AudioClip clip = Sounds.click;
        audioSource.PlayOneShot(clip);
    }
}
