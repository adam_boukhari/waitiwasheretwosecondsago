using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip menuMusic = null;
    [SerializeField] private AudioClip gameMusic = null;

    private AudioSource audioSource = null;

    private void Awake()
    {
        audioSource= GetComponent<AudioSource>();
        audioSource.loop = true;
        audioSource.clip = menuMusic;
        audioSource.Play();
    }

    public void PlayGameMusic()
    {
        audioSource.clip = gameMusic;
        audioSource.Play();
    }

    public void PlayMenuMusic()
    {
        audioSource.clip = menuMusic;
        audioSource.Play();
    }
}
