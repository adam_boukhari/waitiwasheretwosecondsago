using System;
using UnityEngine;

public class Sounds 
{
    //Menu
    public static AudioClip click = Resources.Load("Audio/UI/click") as AudioClip;
    public static AudioClip select = Resources.Load("Audio/UI/select") as AudioClip;
}
