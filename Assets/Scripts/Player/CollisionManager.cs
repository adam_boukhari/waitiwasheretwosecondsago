using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionManager : MonoBehaviour
{
    [SerializeField] private LayerMask layer;

    [SerializeField] private UnityEvent collisionEvents;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((layer.value & 1<<collision.gameObject.layer)>0){
            collisionEvents.Invoke();   
        }
    }

    // Start is called before the first frame update
    void Start()
    {
           
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
