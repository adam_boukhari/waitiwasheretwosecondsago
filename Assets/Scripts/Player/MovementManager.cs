using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(AnimationManager))]
[RequireComponent(typeof(AudioSource))]
public class MovementManager : MonoBehaviour
{
    [Header("Controls")]
    [SerializeField] private float walkGroundSpeed = 0f;
    [SerializeField] private float runGroundSpeed = 0f;
    [SerializeField] private float jumpForce = 0f;
    [SerializeField] private float maxVerticalSpeed = 0f;
    [SerializeField] private bool infiniteJump;
    public LayerMask groundLayers;
    public LayerMask wallLayers;

    [Header("Sounds")]
    [SerializeField] private List<AudioClip> jumpSounds;
    [SerializeField] private AudioClip landSound;

    [Header("Observers")]
    [SerializeField] private List<GameObject> jumpObservers;

    private Rigidbody2D rigidBody = null;
    private AnimationManager anim = null;
    private AudioSource audioSource = null;
    private System.Random randomNumberGenerator = null;

    private float currentSpeed = 0f;
    private float movingSpeed = 0f;
    protected float direction = 0f;
    protected bool isRunning = false;
    private bool wallToTheLeft = false;
    private bool wallToTheRight = false;

    private float coyoteTimer = 0f;
    private float holdJumpTimer = 0f;
    private bool isHoldingJump = false;
    private bool isGrounded = false;
    private bool wasGrounded = false;
    private bool coyoteTimeActive = false;


    private const float MAX_HOLD_JUMP_TIME = 0.25f;
    private const float COYOTE_TIME = 0.1f;
    private const float DOWNWARD_RAYCAST_LENGTH = 0.1f;
    private const float WALL_RAYCAST_LENGTH = 0.225f;
    private const float DOWNWARD_RAYCAST_OFFSET = 0.275f;
    private const float UPWARD_RAYCAST_OFFSET = 0.25f;

    private bool inputEnabled=true;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<AnimationManager>();
        audioSource = GetComponent<AudioSource>();
        randomNumberGenerator = new System.Random();
    }

    private Vector3 startPos;

    private void OnEnable()
    {
        startPos= transform.position;
    }

    private void OnDisable()
    {
        transform.position = startPos;
    }

    private void Update()
    {
        if(inputEnabled)
            HandleInputs();
    }

    private void EnableInput(bool enabled)
    {
        inputEnabled = enabled;
        if (!enabled)
        {
            currentSpeed = 0;
            rigidBody.velocity= new Vector2(0, rigidBody.velocity.y);
            direction = 0;
            movingSpeed = 0;
        }
    }

    private void FixedUpdate()
    {
        HandleCollisions();
        HandleMovement();
        HandleJump();
        HandleAnimations();

        rigidBody.velocity = Vector3.ClampMagnitude(rigidBody.velocity, maxVerticalSpeed);
    }

    private void HandleInputs()
    {
        direction = Input.GetAxis("Horizontal");
        isRunning = Input.GetKey(KeyCode.LeftShift);
        if (Input.GetButtonDown("Jump"))
            OnJumpPressed();
        if(Input.GetButtonUp("Jump"))
            OnJumpReleased();   
    }

    protected void HandleCollisions()
    {
        if (rigidBody.velocity.y > 0f)
        {
            isGrounded = false;
            wasGrounded = false;
            coyoteTimeActive = false;
            coyoteTimer = 0f;
        }

        Vector2 playerFeetPosition = new Vector2(transform.position.x, transform.position.y - DOWNWARD_RAYCAST_OFFSET);
        Vector2 playerHeadPosition = new Vector2(transform.position.x, transform.position.y + UPWARD_RAYCAST_OFFSET);
        if(Physics2D.gravity.y > 0)
        {
            playerFeetPosition = new Vector2(transform.position.x, transform.position.y + UPWARD_RAYCAST_OFFSET);
            playerHeadPosition = new Vector2(transform.position.x, transform.position.y - DOWNWARD_RAYCAST_OFFSET);
        }

        RaycastHit2D hitGround = Physics2D.Raycast(playerFeetPosition, -transform.up, DOWNWARD_RAYCAST_LENGTH, groundLayers);
        
        if(hitGround.collider == null) // No hit
        {
            isGrounded = false;
            if (!isHoldingJump && wasGrounded)
            {
                coyoteTimeActive = true;
                coyoteTimer = COYOTE_TIME;
            }
            wasGrounded = false;
        }
        else if(!isHoldingJump) // Hit
        {
            if (!wasGrounded && rigidBody.velocity.y < 0f)
                audioSource.PlayOneShot(landSound);

            isGrounded = true;
            wasGrounded = true;
            coyoteTimer = 0f;
            coyoteTimeActive = false;
        }

        Vector2 playerPosition = new Vector2(transform.position.x, transform.position.y);

        RaycastHit2D upperLeft = Physics2D.Raycast(playerHeadPosition, Vector2.left, WALL_RAYCAST_LENGTH, wallLayers);
        RaycastHit2D middleLeft = Physics2D.Raycast(playerPosition, Vector2.left, WALL_RAYCAST_LENGTH, wallLayers);
        RaycastHit2D lowerLeft = Physics2D.Raycast(playerFeetPosition, Vector2.left, WALL_RAYCAST_LENGTH, wallLayers);

        RaycastHit2D upperRight = Physics2D.Raycast(playerHeadPosition, Vector2.right, WALL_RAYCAST_LENGTH, wallLayers);
        RaycastHit2D middleRight = Physics2D.Raycast(playerPosition, Vector2.right, WALL_RAYCAST_LENGTH, wallLayers);
        RaycastHit2D lowerRight = Physics2D.Raycast(playerFeetPosition, Vector2.right, WALL_RAYCAST_LENGTH, wallLayers);

        if (upperLeft || middleLeft || lowerLeft)
        {
            wallToTheLeft = true;
            wallToTheRight = false;
        }
        else if(upperRight || middleRight || lowerRight)
        {
            wallToTheLeft = false;
            wallToTheRight = true;
        }
        else
        {
            wallToTheLeft = false;
            wallToTheRight = false;
        }

        // Use these lines for debugging raycast
        Vector2 direction = Vector2.down * DOWNWARD_RAYCAST_LENGTH;
        //Debug.DrawRay(playerFeetPosition, -transform.up * DOWNWARD_RAYCAST_LENGTH, Color.green);
    }

    protected void HandleMovement()
    {
        if (isRunning)
            movingSpeed = runGroundSpeed;
        else
            movingSpeed = walkGroundSpeed;

        if (Physics2D.gravity.y > 0)
            direction *= -1;

        currentSpeed = direction * movingSpeed;

        if (direction > 0f && !wallToTheRight)
            rigidBody.velocity = new Vector2(currentSpeed, rigidBody.velocity.y);
        else if (direction < 0f && !wallToTheLeft)
            rigidBody.velocity = new Vector2(currentSpeed, rigidBody.velocity.y);
        else
            rigidBody.velocity = new Vector2(0f, rigidBody.velocity.y);
    }

    protected void HandleJump()
    {
        if(coyoteTimeActive)
        {
            coyoteTimer -= Time.deltaTime;
            if(coyoteTimer <= 0f)
            {
                coyoteTimeActive = false;
                coyoteTimer = 0f;
            }
        }
        if ((isHoldingJump && (holdJumpTimer <= MAX_HOLD_JUMP_TIME || isGrounded || coyoteTimeActive)))
        {
            holdJumpTimer += Time.deltaTime;
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, Physics2D.gravity.y < 0 ? jumpForce : -jumpForce);
        }
    }

    protected void HandleAnimations()
    {
        anim.SetDirection(Physics2D.gravity.y < 0 ? currentSpeed : -currentSpeed);

        if (!isGrounded)
            anim.SetJumpAnimation();
        else
            anim.SetGroundAnimation();

        if (currentSpeed == 0f)
            anim.SetIdleAnimation();
        else if (isRunning)
            anim.SetRunAnimation();
        else
            anim.SetWalkAnimation();
    }

    protected void OnJumpPressed()
    {
        if (!isGrounded&&!infiniteJump)
            return;
        isHoldingJump = true;
        holdJumpTimer = 0f;

        int index = randomNumberGenerator.Next(1, jumpSounds.Count);
        audioSource.PlayOneShot(jumpSounds[index]);

        foreach (GameObject observer in jumpObservers)
        {
            observer.GetComponent<JumpObserver>().ActionOnJump();
        }
    }

    protected void OnJumpReleased()
    {
        isHoldingJump = false;
        holdJumpTimer = 0f;
    }
}
