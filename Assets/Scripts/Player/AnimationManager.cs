using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private Animator anim;

    public void SetDirection(float speed)
    {
        if (speed == 0) // Dont change direction when releasing movement
            return;

        if (speed < 0)
            sprite.flipX = true;
        else
            sprite.flipX = false;
    }

    public void SetRunAnimation()
    {
        SetAnimationBoolFalse("Walk");
        SetAnimationBoolTrue("Run");
    }

    public void SetWalkAnimation()
    {
        SetAnimationBoolTrue("Walk");
        SetAnimationBoolFalse("Run");
    }

    public void SetIdleAnimation()
    {
        SetAnimationBoolFalse("Walk");
        SetAnimationBoolFalse("Run");
    }

    public void SetDeathAnimation()
    {
        anim.SetTrigger("Death");
    }

    public void SetJumpAnimation()
    {
        SetAnimationBoolTrue("Jump");
        SetAnimationBoolTrue("Fall");
    }

    public void SetGroundAnimation()
    {
        SetAnimationBoolFalse("Jump");
        SetAnimationBoolFalse("Fall");
    }
    public void SetDieAnimation()
    {
        SetAnimationBoolTrue("Die");
    }

    public void RemoveDieAnimation()
    {
        SetAnimationBoolFalse("Die");
    }

    private void SetAnimationBoolTrue(string parameter)
    {
        anim.SetBool(parameter, true);
    }

    private void SetAnimationBoolFalse(string parameter)
    {
        anim.SetBool(parameter, false);
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
