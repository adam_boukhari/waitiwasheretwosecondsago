using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementManager))]

public class DeathManager : MonoBehaviour
{
    private AnimationManager anim;
    private MovementManager movement;

    public LayerMask damageLayer;

    private void Awake()
    {
        anim = GetComponent<AnimationManager>();
        movement = GetComponent<MovementManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((damageLayer.value & 1 << collision.gameObject.layer) > 0)
        {
            anim.SetDeathAnimation();
            LevelManager.Instance.RestartLevel();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((damageLayer.value & 1 << collision.gameObject.layer) > 0)
        {
            anim.SetDeathAnimation();
            LevelManager.Instance.RestartLevel();
        }
    }
}
