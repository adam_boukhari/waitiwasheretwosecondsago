using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class MultiballEndLevelTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent cleanUpEvent;
    [SerializeField] private int ballsRequired;
    private int ballsIn;

    private void OnEnable()
    {
        ballsIn = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ballsIn++;
            if(ballsIn >= ballsRequired)
            {
                LevelManager.Instance.LoadNextLevel();
                if (cleanUpEvent != null) cleanUpEvent.Invoke();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ballsIn--;
        }
    }
}
