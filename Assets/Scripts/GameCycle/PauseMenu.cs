using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pauseScreen = null;

    private bool canPause = false;

    public void SetPauseEnabled(bool enabled)
    {
        Resume();
        canPause = enabled;
    }


    private void Update()
    {
        if (!Input.GetButtonDown("Cancel"))
            return;

        if (pauseScreen.activeSelf)
            Resume();
        else if (canPause)
            Pause();
    }

    private void Pause()
    {
        pauseScreen.SetActive(true);
        Time.timeScale = 0f;
        
    }

    public void Resume()
    {
        pauseScreen.SetActive(false);
        Time.timeScale = 1f;
    }

    public void RestartLevel()
    {
        LevelManager.Instance.RestartLevel();
        Resume();
    }

    public void RestartRun()
    {
        LevelManager.Instance.RestartRun();
        TimerManager.Instance.ResetTimer();
        Resume();
    }

    public void ReturnToMainMenu()
    {
        SetPauseEnabled(false);
        LevelManager.Instance.ReturnToMainMenuScreen();
        TimerManager.Instance.HideTimer();
        Resume();
    }
}
