using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(MusicPlayer))]
public class LevelManager : MonoBehaviour
{
    [SerializeField] private PauseMenu pauseMenu = null;
    [SerializeField] private List<LevelData> levels = new List<LevelData>();
    [SerializeField] private Transition transitionScreen;

    public static LevelManager Instance = null;
    private bool transition = false;
    private MusicPlayer musicPlayer = null;

    private int currentLevelIndex = -1;

    [Serializable]
    public struct LevelData
    {
        public string name;
        public LevelStarter level;
    }

    private void Awake()
    {
        if(Instance == null)
            Instance = this;
        else if(Instance != this)
            Destroy(Instance);

        musicPlayer = GetComponent<MusicPlayer>();

        foreach(LevelData level in levels)
            level.level.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        transitionScreen.OnTranstionFinished += StartCurrentLevel;
        transitionScreen.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !transition)
        {
            transition = true;
            RestartLevel();
        }
    }

    private void OnDisable()
    {
        transitionScreen.OnTranstionFinished -= StartCurrentLevel;
    }

    public void LoadNextLevel()
    {
        transitionScreen.gameObject.SetActive(true);
        currentLevelIndex++;

        if (currentLevelIndex < levels.Count)
        {
            if(currentLevelIndex <= 0)
            {
                transitionScreen.RollTransition(MainMenu.Instance.GetScreen(), levels[currentLevelIndex].level.gameObject, levels[currentLevelIndex].name);
                pauseMenu.SetPauseEnabled(true);
                musicPlayer.PlayGameMusic();
            }
            else
                transitionScreen.RollTransition(levels[currentLevelIndex - 1].level.gameObject, levels[currentLevelIndex].level.gameObject, levels[currentLevelIndex].name);
            TimerManager.Instance.StartTimer();
        }
        else
            ShowEndScreen();
    }

    public void ReturnToMainMenuScreen()
    {
        musicPlayer.PlayMenuMusic();
        transitionScreen.RollTransition(levels[currentLevelIndex].level.gameObject, MainMenu.Instance.GetScreen());
        currentLevelIndex = -1;
        pauseMenu.SetPauseEnabled(false);
    }

    public void RestartLevel()
    {
        GameObject level = levels[currentLevelIndex].level.gameObject;
        transitionScreen.RollTransition(level, level, levels[currentLevelIndex].name);
    }

    public void RestartRun()
    {
        GameObject level = levels[currentLevelIndex].level.gameObject;
        currentLevelIndex = 0;
        transitionScreen.RollTransition(level, levels[currentLevelIndex].level.gameObject, levels[currentLevelIndex].name);
    }

    private void ShowEndScreen()
    {
        musicPlayer.PlayMenuMusic();
        levels[currentLevelIndex - 1].level.gameObject.SetActive(false);
        currentLevelIndex = -1;
        pauseMenu.SetPauseEnabled(false);
        MainMenu.Instance.EndScreen(TimerManager.Instance.GetTime());
        TimerManager.Instance.HideTimer();
    }

    private void StartCurrentLevel()
    {
        transition = false;
        if (currentLevelIndex > -1 && currentLevelIndex < levels.Count)
            levels[currentLevelIndex].level.StartLevel();
    }
}
