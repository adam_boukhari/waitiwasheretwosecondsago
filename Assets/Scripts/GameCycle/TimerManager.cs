using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI timerText = null;
    [SerializeField] private Image backGround = null;

    public static TimerManager Instance = null;

    private float timer = 0f;
    private bool timerActive = false;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(Instance);
    }

    public void PauseTimer()
    {
        timerActive = false;
    }

    public void StartTimer()
    {
        timerActive = true;
        backGround.gameObject.SetActive(true);
        timerText.gameObject.SetActive(true);
    }

    public string GetTime()
    {
        timerActive = false;
        return Math.Round(timer, 2).ToString();
    }

    public void ResetTimer()
    {
        timer = 0f;
    }

    public void HideTimer()
    {
        timerActive = false;
        timer = 0f;
        backGround.gameObject.SetActive(false);
        timerText.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (!timerActive)
            return;

        timer += Time.deltaTime;
        timerText.text = Math.Round(timer, 2).ToString();
    }

}
