using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Transition : MonoBehaviour
{
    [SerializeField] private Animator levelNameAnim = null;
    [SerializeField] private TMPro.TextMeshProUGUI levelTitle = null;

    public Action OnTranstionFinished = null;

    private Animator anim = null;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void RollTransition(GameObject previousLevel, GameObject nextLevel, string nextLevelName = "")
    {
        levelTitle.text = nextLevelName;
        anim.SetBool("Transition", true);
        StartCoroutine(WaitForLevelSwitch(previousLevel, nextLevel, nextLevelName != ""));
        StartCoroutine(WaitForFade());
    }

    private IEnumerator WaitForLevelSwitch(GameObject previous, GameObject next, bool showName)
    {
        yield return new WaitForSeconds(0.2f);
        if(showName)
            levelNameAnim.SetTrigger("Transition");
        previous?.SetActive(false);
        next?.SetActive(true);
    }

    private IEnumerator WaitForFade()
    {
        yield return new WaitForSeconds(0.4f);
        anim.SetBool("Transition", false);
        OnTranstionFinished?.Invoke();
    }
}
