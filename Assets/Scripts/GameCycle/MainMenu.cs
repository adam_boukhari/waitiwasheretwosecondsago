using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu = null;
    [SerializeField] private GameObject playMenu = null;
    [SerializeField] private GameObject controlsMenu = null;
    [SerializeField] private GameObject aboutMenu = null;
    [SerializeField] private GameObject endMenu = null;
    [SerializeField] private TMPro.TextMeshProUGUI endTimer = null;
    [SerializeField] private Animator bookAnimator = null;
    [SerializeField] private GameObject menusParent = null;

    private GameObject currentMenu = null;

    public static MainMenu Instance = null;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(Instance);
    }


    public void Warning()
    {
        ChangeMenu(playMenu, mainMenu, false);
    }

    public void Play()
    {
        LevelManager.Instance.LoadNextLevel();
    }

    public void Controls()
    {
        ChangeMenu(controlsMenu, mainMenu, false);
    }

    public void About()
    {
        ChangeMenu(aboutMenu, mainMenu, false);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void EndScreen(string time)
    {
        menusParent.SetActive(true);
        playMenu.SetActive(false);
        playMenu.SetActive(false);
        ChangeMenu(endMenu, endMenu, false);
        endTimer.text = time;
    }

    public GameObject GetScreen()
    {
        return menusParent;
    }

    public void Back()
    {
        if(currentMenu != null && currentMenu != mainMenu)
            ChangeMenu(mainMenu, currentMenu, true);
    }

    private void Update()
    {
        if (menusParent.activeSelf && Input.GetButtonDown("Cancel"))
            Back();
    }

    private void ChangeMenu(GameObject menuToSwitchTo, GameObject menuToSwitchFrom, bool turnRight)
    {
        currentMenu = menuToSwitchTo;
        menuToSwitchFrom?.SetActive(false);

        if (turnRight)
        {
            bookAnimator.SetTrigger("TurnRight");
        }
        else
        {
            bookAnimator.SetTrigger("TurnLeft");
        }
        StartCoroutine(WaitForPage(menuToSwitchTo));
    }
    IEnumerator WaitForPage(GameObject menuToSwitchTo)
    {
        yield return new WaitForSeconds(0.5f);
        menuToSwitchTo?.SetActive(true);
    }

    public void ShowMainMenu()
    {
        menusParent.SetActive(true);
        mainMenu.SetActive(true);
        playMenu.SetActive(false);
        endMenu.SetActive(false);
    }
}
