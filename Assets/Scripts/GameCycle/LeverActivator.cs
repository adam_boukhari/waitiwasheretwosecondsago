using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.Universal;

public class LeverActivator : MonoBehaviour
{
    [SerializeField] private UnityEvent triggerredEvent;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Light2D light2D;
    [SerializeField] private Color deactivatedColor;
    [SerializeField] private Color activatedColor;
    private bool activated = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!activated && collision.CompareTag("Player"))
        {
            spriteRenderer.flipX = true;
            light2D.color = activatedColor;

            activated = true;
            triggerredEvent.Invoke();
        }   
    }

    private void OnEnable()
    {
        activated = false;
        light2D.color = deactivatedColor;
    }
}
