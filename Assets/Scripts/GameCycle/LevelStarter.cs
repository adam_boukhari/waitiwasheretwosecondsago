using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStarter : MonoBehaviour
{
    [SerializeField] private GameObject player = null;

    public Action OnLevelStarted = null;

    private void Awake()
    {
        if(player != null)
            player.SetActive(false);
    }

    public void StartLevel()
    {
        if (player != null)
            player.SetActive(true);
        OnLevelStarted?.Invoke();
    }
}
